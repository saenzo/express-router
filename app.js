// Node modules
const express = require('express')
const createError = require('http-errors')
const path = require('path')
const cookieParser = require('cookie-parser')
const logger = require('morgan')
const cors = require('cors')
const compression = require('compression')
const helmet = require('helmet')

//const config = require('./config.json')

const app = express()

app.set('trust proxy', 1)
app.set('view engine', 'html');
app.use(cors())
app.use(compression())
app.use(
    helmet({
        referrerPolicy: {
            policy: 'no-referrer'
        },
        contentSecurityPolicy: false
    })
)
app.use(logger('dev'))
app.use(express.json())
app.use(express.urlencoded({
    extended: false
}))
app.use(cookieParser())
app.use(express.static(path.join(__dirname, 'build')))

require('./middlewares')(app)
require('./bin/routes')(app)

// Catch 404 and forward to error handler
app.use((req, res, next) => {
    next(createError(404))
})

// Error handler
app.use((err, req, res) => {
    // set locals, only providing error in development
    res.locals.message = err.message
    res.locals.error = req.app.get('env') === 'development' ? err : {}

    // render the error page
    res.status(err.status || 500)

    res.json(err)
})

module.exports = app
