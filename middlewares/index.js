/**
 * * Middlewares
 * *
 *   !order is important
 * 
 */

module.exports = app => {
    // Middleware global
    // Normalizar respuestas para peticiones
    app.use('/', require('./response'))

    // Load custom middlewares here..
    // ...
}
