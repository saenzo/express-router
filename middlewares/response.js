/**
 * * Responder peticiones
 * *
 *   Normalizar la respuesta de peticiones
 *   de todas las solicitudes
 * 
 */

const getResult = result => {
    if (result.result) {
        return result.result
    }

    return result
}

const getError = code => {
    let message = ''

    return { message: message, code: code }
}

module.exports = (req, res, next) => {
    /**
     * 
     * @param {string} err - Mensaje de error
     * @param {any} result - Payload / Status code
     */

    req.response = (err, result) => {        
        // Establecer el estatus si esta definido
        if (err && !result && Number.isInteger(result)) {
            res.status(result)
        }

        if (err) {
            res.json({
                success: false,
                error: getError(err)
            })
        } else {
            res.json({
                success: true,
                payload: getResult(result)
            })
        }
    }

    next()
}
