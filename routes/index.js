/**
 * * Serving Apps with Client-Side Routing
 * *
 *   https://create-react-app.dev/docs/deployment/
 * 
 */

const path = require('path')
const dirname = __dirname.replace('routes', '')
const { build_dir } = require('../config.json')

module.exports = router => {
    router.get('/*', (req, res) => {
        let htmlfile = path.join(dirname, build_dir, 'index.html')
        
        res.sendFile(htmlfile)
    })

    return router
}
