

module.exports = router => {

    router.get('/', (req, res, next) => {
        req.response(null, 'Users listing...')
    })

    return router
}